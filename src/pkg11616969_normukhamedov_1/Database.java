/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_normukhamedov_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Helkern
 */
public class Database {
    private static Connection conn;
    
    public static Connection getConn(){
        if(conn == null){
            try{
                String url = "jdbc:mysql://localhost:3306/student"; // string with the actual database name
                DriverManager.registerDriver(new com.mysql.jdbc.Driver()); //service for managing a set of JDBC drivers, registering new driver instance
                conn = (com.mysql.jdbc.Connection) DriverManager.getConnection(url, "root", "ProBook4540s!"); // Connecting to the database
                System.out.print("Successfully connected to the database");
            }
            catch(SQLException e){
                System.out.print("Unable to connect to the database");
            }
        }
        return conn;
    }
    
    public static void main(String[] args){
        getConn();
    }
}
