/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_normukhamedov_1;

/**
 *
 * @author Helkern
 */
public class Student {
    // private properties
    private int studentID;
    private String studentName;
    private int quiz;
    private int cw1;
    private int cw2;
    private int cw3;
    private int exam;
    private double result;
    private String grade;

    public Student(String studentId, String studentName, String quiz, String cw1, String cw2, String cw3, String exam) throws Exception {
        // Only after validation private fields will be populated, if not valid jumps to the View class on the exception throw
        if(studentId.equals(""))
            throw new Exception("Student Id cannot be null");
        else if(studentName.equals(""))
            throw new Exception("Student Name cannot be null");
        else if(quiz.equals(""))
            throw new Exception("Quiz cannot be null");
        else if(cw1.equals(""))
            throw new Exception("CW1 cannot be null");
        else if(cw2.equals(""))
            throw new Exception("CW2 cannot be null");
        else if(cw3.equals(""))
            throw new Exception("CW3 cannot be null");
        else if(exam.equals(""))
            throw new Exception("Exam cannot be null");
            
        if (!(studentId.length() == 8)) {
            throw new Exception("Invalid student ID");
        }
        this.studentID = Integer.parseInt(studentId);
        this.studentName = studentName;
        this.quiz = Integer.parseInt(quiz);
        if (!(this.quiz >= 0 & this.quiz <= 100)) {
            throw new Exception("Invalid quiz only 0-100");
        }
        this.cw1 = Integer.parseInt(cw1);
        if (!(this.cw1 >= 0 & this.cw1 <= 100)) {
            throw new Exception("Invalid cw1 only 0-100");
        }
        this.cw2 = Integer.parseInt(cw2);
        if (!(this.cw2 >= 0 & this.cw2 <= 100)) {
            throw new Exception("Invalid cw2 only 0-100");
        }
        this.cw3 = Integer.parseInt(cw3);
        if (!(this.cw3 >= 0 & this.cw3 <= 100)) {
            throw new Exception("Invalid cw3 only 0-100");
        }
        this.exam = Integer.parseInt(exam);
        if (!(this.exam >= 0 & this.exam <= 100)) {
            throw new Exception("Invalid exam only 0-100");
        }
        this.result = calculateResult();
        this.grade = calculateGrade(this.result);
    }

    private double calculateResult() { // calculating result as to the requirements
        double result = (this.getQuiz() * 0.05) + (this.getCw1() * 0.15) + (this.getCw2() * 0.2) + (this.getCw3() * 0.10) + (this.getExam() * 0.5);
        return result;
    }

    private String calculateGrade(double result) { // setting degree as to the requirements
        if (result >= 85) {
            return "HD";
        } else if (result <= 75 && result > 85) {
            return "DI";
        } else if (result >= 65 && result < 75) {
            return "CR";
        } else if (result >= 50 & result < 65) {
            return "PS";
        } else {
            return "FL";
        }
    }

    /**
     * @return the studentID
     */
    public int getStudentID() {
        return studentID;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @return the quiz
     */
    public int getQuiz() {
        return quiz;
    }

    /**
     * @return the cw1
     */
    public int getCw1() {
        return cw1;
    }

    /**
     * @return the cw2
     */
    public int getCw2() {
        return cw2;
    }

    /**
     * @return the cw3
     */
    public int getCw3() {
        return cw3;
    }

    /**
     * @return the exam
     */
    public int getExam() {
        return exam;
    }

    /**
     * @return the result
     */
    public double getResult() {
        return result;
    }

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param studentID the studentID to set
     */
    public void setStudentID(String studentID) throws Exception {
        if (studentID.equals("")) {
            throw new Exception("Student Id cannot be empty");
        }
        this.studentID = Integer.parseInt(studentID);
    }

}
